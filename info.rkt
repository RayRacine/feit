#lang info
(define collection "feit")
(define deps '("bitsyntax" "word" "typed-racket-more" "typed-racket-lib" "base" ))
(define build-deps '("scribble-lib" "typed-racket-doc" "racket-doc" "rackunit-lib"))
(define scribblings '(("scribblings/feit.scrbl" ())))
(define pkg-desc "Protocol to controll Feit Homebrite(TM) Bluetooth bulbs")
(define version "0.1.0")
(define pkg-authors '("Raymond Racine"))

